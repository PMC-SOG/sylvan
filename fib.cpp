// fib.c
#include <fib.h>
TASK_IMPL_1(int, fib, int, n)
{
    if (n<2) return n;
    SPAWN(fib, n-1);
    SPAWN(fib, n-2);
    int fib_minus_2 = SYNC(fib);
    // Alternatively, instead of SPAWN/SYNC, use:
    // int fib_minus_2 = CALL(fib, n-2);
    int fib_minus_1 = SYNC(fib);
    return fib_minus_1 + fib_minus_2;
}
