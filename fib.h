#ifndef FIB_H_INCLUDED
#define FIB_H_INCLUDED

#include <lace.h>
TASK_DECL_1(int, fib, int);
#define fib(n) CALL(fib, (n))



#endif // FIB_H_INCLUDED
